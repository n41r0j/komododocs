Komodo docs
===========


Header
======

Here is some text explaining some very important stuff.::

    print 'hello'
    >> hello

Guide
^^^^^

.. toctree::
   :maxdepth: 2

   license
   help


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
